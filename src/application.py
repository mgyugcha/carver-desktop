import gi
import sys

gi.require_version('Gtk', '3.0')
from gi.repository import Gio, Gtk

from carver.sorter_window import SorterWindow
from carver.recover_window import RecoverWindow

class Window():
    def __init__(self, app):
        resource = "/com/gitlab/mgyugcha/carver/main.glade"
        try:
            builder = Gtk.Builder.new_from_resource(resource)
            builder.connect_signals(self)
        except GObject.GError:
            print("Error al cargar el archivo glade")
            raise
        self.__window = builder.get_object("window")
        self.__window.set_application(app)
        self.__application = app

        self.__radio_form = builder.get_object("radio_button_form")
        self.__radio_recover = builder.get_object("radio-button-recover")
        self.__radio_sorter = builder.get_object("radio-button-sorter")

    def get_window(self):
        return self.__window

    def on_button_select_clicked(self, widget):
        if self.__radio_form.get_active():
            print('mostrar form')
        elif self.__radio_recover.get_active():
            self.__application.show_recover_window()
        else:
            self.__application.show_sorter_window()

class Application(Gtk.Application):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id="com.gitlab.mgyugcha.carver",
                         flags=Gio.ApplicationFlags.FLAGS_NONE,
                         **kwargs)
        self.__window = None

    def do_startup(self):
        Gtk.Application.do_startup(self)
        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)

    def do_activate(self):
        self.show_sorter_window()
        # if not self.__window:
        #     w = Window(self)
        #     self.__window = w.get_window()
        # self.__window.present()

    def on_quit(self, action, param):
        self.quit()

    def show_main_window(self):
        if self.__window:
            self.__window.destroy()
        w = Window(self)
        self.__window = w.get_window()
        self.__window.present()

    def show_recover_window(self):
        if self.__window: self.__window.destroy()
        try:
            w = RecoverWindow(self)
            self.__window = w.get_window()
            self.__window.present()
        except:
            print(sys.exc_info()[0].message)
            print('El usuario no se autentificó como administrador')

    def show_sorter_window(self, input_folder=None):
        if self.__window: self.__window.destroy()
        w = SorterWindow(self, input_folder)
        self.__window = w.get_window()
        self.__window.present()
