from gi.repository import Gtk

def warning(message, parent=None):
    dialog = Gtk.MessageDialog(parent,
                               Gtk.DialogFlags.USE_HEADER_BAR,
                               Gtk.MessageType.WARNING,
                               Gtk.ButtonsType.OK,
                               message)
    dialog.set_title('Advertencia')
    result = dialog.run()
    dialog.destroy()

def info(message, parent=None):
    dialog = Gtk.MessageDialog(parent,
                               Gtk.DialogFlags.USE_HEADER_BAR,
                               Gtk.MessageType.INFO,
                               Gtk.ButtonsType.OK,
                               message)
    dialog.set_title('Información')
    result = dialog.run()
    dialog.destroy()
