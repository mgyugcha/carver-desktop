import gi

gi.require_version('Gtk', '3.0')

from gi.repository import GLib, Gio, Gtk

from carver import dialogs
from carver import file_sorter

class SorterWindow(Gtk.ApplicationWindow):
    def __init__(self, app, input_folder):
        try:
            resource = "/com/gitlab/mgyugcha/carver/file_sorter.glade"
            builder = Gtk.Builder.new_from_resource(resource)
            builder.connect_signals(self)
        except GObject.GError:
            print("Error reading GUI file")
            raise

        self.__app = app

        self.__notebook = builder.get_object("notebook")
        self.__progress = builder.get_object("progress_bar")

        self.__list_positive = builder.get_object("list_positive")
        self.__list_negative = builder.get_object("list_negative")
        self.__list_other = builder.get_object("list_other")
        self.__action_button = builder.get_object("action_button")
        self.__cancel_button = builder.get_object("cancel_button")

        self.__input_file_chooser_button = builder.get_object("input_folder")
        self.__output_file_chooser_button = builder.get_object("output_folder")
        self.input_folder = '/home/mgyugcha/output/' # input_folder
        if (input_folder):
            self.__input_file_chooser_button.set_filename(input_folder)
        self.output_folder = '/home/mgyugcha/out-test/' # None

        self.__window = builder.get_object("window")
        self.__window.set_application(app)

    def on_input_folder_file_set (self, button):
        self.input_folder = button.get_filename()
        # self.do_sensitive_action_button()

    def on_output_folder_file_set (self, button):
        self.output_folder = button.get_filename()
        # self.do_sensitive_action_button()

    def do_sensitive_action_button (self):
        sensitive = self.input_folder is not None and self.output_folder is not None
        # self.__action_button.set_sensitive(sensitive)

    def __correr(self):
        print('hilo')

    def __success_run_sorter(self, source, res, data):
        print('cancelado', res, data)

    def action_button_clicked_cb (self, button):
        self.__action_button.set_visible(False)
        self.__cancel_button.set_visible(True)
        # self.__progress.pulse()
        self.__cancellable = Gio.Cancellable.new()
        data = self.input_folder, self.output_folder, self.__progress
        file_sorter.run_sorter_async(
            self.__cancellable,
            self.__success_run_sorter,
            (self.input_folder, self.output_folder, self.__progress))
        print('terminó')
#         self.loading(True)
#         result = check_directory(self.input_folder, self.output_folder)
#         for i in range(len(result["positivos"])):
#             self.__list_positive.append([result["positivos"][i]])
#         for i in range(len(result["negativos"])):
#             self.__list_negative.append([result["negativos"][i]])
#         for i in range(len(result["otros"])):
#             self.__list_other.append([result["otros"][i]])
#         dialogs.info('''Se ordenaron los archivos correctamente
# Positivos: %d
# Negativos: %d
# Sin clasificación: %d''' % (len(self.__list_positive),
#                             len(self.__list_negative),
#                             len(self.__list_other)))
#         self.loading(False)



    def loading (self, status):
        self.__action_button.set_sensitive(not status)
        self.__input_file_chooser_button.set_sensitive(not status)
        self.__output_file_chooser_button.set_sensitive(not status)
        # self.__loader.set_visible(status)
        # if status:
        #     self.__loader.start()
        # else:
        #     self.__loader.stop()

    def get_window(self):
        return self.__window
