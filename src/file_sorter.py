from threading import Thread
import os
import re
import json
import mimetypes
from shutil import copyfile

import gi
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GdkPixbuf, Gio

with open('/home/mgyugcha/Documents/Projects/Tesis/carver-desktop/data/file_signatures.json') as f:
    Signatures = json.load(f)
    for filetype in Signatures:
        for signature in Signatures[filetype]:
            signature['re'] = re.compile(signature['hex'], re.IGNORECASE)
        

def _get_divisor (number):
    """Obtiene el número que será tomado de referencia para incrementar el
    progress_bar

    """
    count = 0
    while (number > 0):
        number = number // 10
        count = count + 1
    return 10 ** int(count/2)

def _get_files_in_dir(dirname):
    """Retorna una lista con todos los archivos de un directorio y sus
    subdirectorios.

    """
    files = [os.path.join(dirname, f) for f in os.listdir(dirname)]
    falses = []
    only_files = []
    for f in files:
        if (os.path.isfile(f)):
            only_files.append(f)
        elif (os.path.isdir(f)):
            result = _get_files_in_dir(f)
            only_files.extend(result[0])
            falses.extend(result[1])
        else:
            falses.append(f)
    return only_files, falses

def _match_header (bites):
    for filetype in Signatures:
        for signature in Signatures[filetype]:
            if signature['re'].match(bites):
                return filetype
    return 'plain'

def _sort_files (filename, filetype, sorted_files):
    files = sorted_files.get(filetype)
    if (files):
        files.append(filename)
    else:
        sorted_files[filetype] = [filename]

def _proccess_file (f, filetype, outputdir, result):
    basedir = os.path.join(outputdir, filetype)
    good_dir = os.path.join(basedir, 'buenos')
    bad_dir = os.path.join(basedir, 'malos')
    if (filetype == 'image'):
        try:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(f)
            if (not os.path.exists(basedir)): os.mkdir(basedir)
            if (not os.path.exists(good_dir)): os.mkdir(good_dir)
            tmp = os.path.join(good_dir, os.path.basename(f))
            copyfile(f, tmp)
            result['positivos'].append(tmp)
        except:
            if (not os.path.exists(bad_dir)): os.mkdir(bad_dir)
            tmp = os.path.join(bad_dir, os.path.basename(f))
            copyfile(f, tmp)
            result['negativos'].append(tmp)
            print('Archivo erroneo:', f)

def _check_directory(inputdir, outputdir, progress):
    files, falses = _get_files_in_dir(inputdir)
    sorted_files = dict()
    size = len(files)
    divisor = _get_divisor(size)
    result = { 'positivos': [], 'negativos': [], 'otros': [] }
    for i, f in enumerate(files):
        if ((i+1) % divisor == 0):
            fraction = (i+1) / size
            progress.set_text('Clasificando archivos (%s %%)'
                              % str(round(fraction * 100, 2)))
            progress.set_fraction(fraction)
        try:
            with open(f, 'rb') as g:
                bite = g.read(20).hex()
                filetype = _match_header(bite)
                # _sort_files(f, filetype, sorted_files)
                _proccess_file(f, filetype, outputdir, result)
        except:
            print('error')
    # print(sorted_files)
    return 0

def _get_files(task, data):
    inputdir, outputdir, progress = data
    progress.set_visible(True)
    progress.set_show_text(True)
    progress.set_text('Analizando directorio')
    result = _check_directory(inputdir, outputdir, progress)
    print('esta vez terminó')
    # print(len(result))

def run_sorter_async(cancellable, callback, data):
    task = Gio.Task.new(None, cancellable, callback, None)
    thread = Thread(target=_get_files, args=(task, data))
    thread.start()
