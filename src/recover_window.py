import gi
import os
import warnings
from carver import dialogs
from distutils.spawn import find_executable

gi.require_version('Polkit', '1.0')
gi.require_version('Gtk', '3.0')
gi.require_version('Vte', '2.91')

from gi.repository import GLib, Gio, Gtk, Vte, Polkit, GObject

PKEXEC = find_executable('pkexec')

class RecoverWindow():
    def __init__(self, app):
        self.__authorized = self.__get_is_authorized()
        try:
            resource = "/com/gitlab/mgyugcha/carver/recover.glade"
            builder = Gtk.Builder.new_from_resource(resource)
            builder.connect_signals(self)
        except GObject.GError:
            print("Error al cargar el archivo glade")
            raise

        self.__app = app

        self.__combo_box_mounts = builder.get_object("combo_box_mounts")
        self.__liststore_mounts = builder.get_object("liststore_mounts")
        volume_monitor = Gio.VolumeMonitor.get()
        self.on_mount_added(volume_monitor, None)
        volume_monitor.connect("mount-added", self.on_mount_added)
        volume_monitor.connect("mount-removed", self.on_mount_added)

        self.__combo_box_carvers = builder.get_object('combo_box_carvers')
        self.__liststore_carvers = builder.get_object("liststore_carvers")
        self.get_installed_carvers()

        self.__file_chooser = builder.get_object("file_chooser_button_output")
        self.__action_button = builder.get_object("button_recover")

        self.box = builder.get_object("box")
        self.__terminal = Vte.Terminal.new()
        self.__terminal.connect('child-exited', self.on_child_exited)

        self.box.pack_start(self.__terminal, True, True, 0)
        self.__terminal.set_scrollback_lines(1000)
        self.__terminal.set_audible_bell(False)
        self.__terminal.show()

        self.__window = builder.get_object("window")
        self.__window.set_application(app)

    def on_mount_added (self, volume_monitor, mount):
        self.__mounts = volume_monitor.get_mounts()
        self.update_mounts()

    def update_mounts(self):
        icon_theme = Gtk.IconTheme.get_default()
        self.__liststore_mounts.clear()
        for mount in self.__mounts:
            icon = mount.get_icon()
            symbolic_name = icon.get_names()[0]
            name = mount.get_name()
            try:
                drive = mount.get_drive().get_identifier("unix-device")
            except:
                print("El dispositivo %s no se pudo conectar" % name)
                continue
            pixbuf = icon_theme.load_icon(symbolic_name, 16, 0)
            self.__liststore_mounts.append([pixbuf, name, drive])

    def get_installed_carvers(self):
        # scalpel
        if find_executable('scalpel') is not None:
            executable = find_executable('scalpel')
            self.__liststore_carvers.append(['Scalpel', executable])
        else:
            warnings.warn('No se encotró scalpel')

    def get_window(self):
        return self.__window

    def __get_is_authorized(self):
        action_id = "org.freedesktop.policykit.exec"
        authority = Polkit.Authority.get_sync()
        subject = Polkit.UnixProcess.new_for_owner(os.getppid(), 0, -1)
        result = authority.check_authorization_sync(
            subject,
            action_id,
            None,
            Polkit.CheckAuthorizationFlags.NONE
        )
        return result.get_is_authorized()

    def button_recover_clicked_cb(self, button):
        # carver
        carver_iter = self.__combo_box_carvers.get_active_iter()
        if (not carver_iter):
            message = '''
Seleccione la aplicación carver con la que desea recuperar la
información'''
            return dialogs.warning(message, self.__window)
        carver = self.__liststore_carvers.get_value(carver_iter, 1)
        # mount
        mount_iter = self.__combo_box_mounts.get_active_iter()
        if (not mount_iter):
            message = '''
Seleccione la unidad extraible de la cual se quiere recuperar los
archivos'''
            return dialogs.warning(message, self.__window)
        mount = self.__liststore_mounts.get_value(mount_iter, 2)
        # output
        output = self.__file_chooser.get_filename()
        if (not output):
            return dialogs.warning("Seleccione una carpeta de salida",
                                   self.__window)
        self.__action_button.set_sensitive(False)
        commands = [carver, mount, '-o', output, None]
        if (not self.__authorized): commands = [PKEXEC] + commands
        self.__run_carver(commands)
        # self.__run_carver([find_executable('wget'), 's', None])

    def __run_carver (self, commands):
        print('running:', commands)
        pty = Vte.Pty.new_sync(Vte.PtyFlags.DEFAULT, None)
        self.__terminal.set_pty(pty)
        pty.spawn_async(None,
                        commands,
                        None,
                        GLib.SpawnFlags.DO_NOT_REAP_CHILD,
                        None,
                        None,
                        -1,
                        None,
                        self.vte_async_ready,
                        None)

    def on_child_exited(self, terminal, status):
        self.__action_button.set_sensitive(True)
        if (status == 0):
            dialogs.info('Se recuperaron los archivos correctamente')
            output = self.__file_chooser.get_filename()
            self.__app.show_sorter_window(output)
        elif (status == 32256):
            dialogs.warning('''
Es necesario autentificarse como administrador para continuar''',
            self.__window)
        else:
            dialogs.warning('''
Ocurrió un problema, por favor verifique los mensajes de error''',
            self.__window)

    def vte_async_ready(self, vte, task, data):
        result = vte.spawn_finish(task)
        self.__terminal.watch_child(result[1])
